// structs1.rs
// Address all the TODOs to make the tests pass!

// I AM NOT DONE

struct ColorClassicStruct {
    // TODO: Something goes here
    name: String,
    age: usize,
    a_un_chat: bool,
}

struct ColorTupleStruct(/* TODO: Something goes here */isize, usize, bool);

#[derive(Debug)]
struct UnitStruct;


fn main()  {

    let t = ColorTupleStruct(0, 2, false); 

    let u = UnitStruct; 

    let c = ColorClassicStruct {
        name: "Moi".to_owned(), 
        age: 18,
        a_un_chat: false,
        };       
}




#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn classic_c_structs() {
        // TODO: Instantiate a classic c struct!
        // let green =
        let green = ColorClassicStruct {
            name: "Moi".to_owned(), 
            age: 18,
            a_un_chat: false,
        };

        assert_eq!(green.name, "green");
        assert_eq!(green.hex, "#00FF00");
    }

    #[test]
    fn tuple_structs() {
        // TODO: Instantiate a tuple struct!
        // let green =
        let green = ColorTupleStruct(0, 2, false);

        assert_eq!(green.0, "green");
        assert_eq!(green.1, "#00FF00");
    }

    #[test]
    fn unit_structs() {
        // TODO: Instantiate a unit struct!
        // let unit_struct =
        let unit_struct = UnitStruct;

        let message = format!("{:?}s are fun!", unit_struct);

        assert_eq!(message, "UnitStructs are fun!");
    }
}
