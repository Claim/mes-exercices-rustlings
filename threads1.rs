// threads1.rs
// Make this compile! Execute `rustlings hint threads1` for hints :)
// The idea is the thread spawned on line 21 is completing jobs while the main thread is
// monitoring progress until 10 jobs are completed. If you see 6 lines
// of "waiting..." and the program ends without timing out when running,
// you've got it :)

// I AM NOT DONE


use std::sync::{Arc, Mutex};
use std::thread;

struct JobStatus {
    jobs_completed: u32,
}

fn main() {
    let status = Arc::new(Mutex::new(JobStatus { jobs_completed: 0 }));
    let status_shared = status.clone();
    thread::spawn(move || {
        for _ in 0..10 {
            thread::sleep_ms(250);
            let mut status_shared = status_shared.lock().unwrap();
            status_shared.jobs_completed += 1;
        }
    });
    let status_check = status.clone();
    let mut jobs_completed = {
        let s = status_check.lock().unwrap();
        s.jobs_completed
    };
    while jobs_completed < 10 {
        println!("waiting... {}", jobs_completed);
        thread::sleep_ms(500);
        jobs_completed = {
            let s = status_check.lock().unwrap();
            s.jobs_completed
        };
    }
}
