// enums1.rs
// Make me compile! Execute `` for hints!

// I AM NOT DONE

#[derive(Debug)]
enum Message {
    // TODO: define a few types of messages as used below
    
    Quit,
    Echo,
    Move,
    ChangeColor,
}

fn main() {
    let q = Message::Quit;
    let e = Message::Echo;
    let m = Message::Move;
    let cc =  Message::ChangeColor;

    println!("{:?}", /*Message::Quit*/q);
    println!("{:?}", /*Message::Echo*/e);
    println!("{:?}", /*Message::Move*/m);
    println!("{:?}", /*Message::ChangeColor*/cc);
}
